﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Builders.DotNetCore.Domain;
using Microsoft.AspNetCore.Mvc;

namespace Builders.DotNetCore.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class PalindromeController : Controller
    {
        readonly Palindrome palindrome;
        public PalindromeController()
        {
            palindrome = new Palindrome();
        }

        [HttpGet("{word}")]
        public IActionResult IsPalindrome(string word)
        {
            if (!Regex.Match(word, "^[a-zA-Z]*$").Success)
                return BadRequest("invalid input");

            var result = palindrome.IsPalindrome(word);

            return Ok(new { word, result });
        }
    }
}