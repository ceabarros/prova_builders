﻿using Builders.DotNetCore.Data;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Builders.DotNetCore.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        private UnitOfWork _unitOfWork;

        public ValuesController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        
        [HttpGet]
        public IActionResult Get()
        {            
            return Ok("Welcome to Builders!");
        }
        
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            return Ok($"Welcome to Builders! Your lucky number is {id}.");
        }
        
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]string value)
        {
            throw new NotImplementedException();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody]string value)
        {
            throw new NotImplementedException();
        }
        
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}
