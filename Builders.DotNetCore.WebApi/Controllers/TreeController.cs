﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Builders.DotNetCore.Data;
using Builders.DotNetCore.Domain;
using Microsoft.AspNetCore.Mvc;

namespace Builders.DotNetCore.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class TreeController : Controller
    {
        private readonly Repository<TreeChain> repository;

        public TreeController(UnitOfWork unitOfWork) => repository = unitOfWork.GetRepository<TreeChain>();

        [HttpGet]
        public IActionResult Get()
        {
            var tree = repository.ToList();

            if (tree.Any())
                return Ok(tree);

            return NotFound("No tree was found");
        }

        [HttpGet("{value}")]
        public IActionResult FindWithValue(int value)
        {
            var result = new List<TreeChain>();
            var treeList = repository.ToList();

            if (treeList.Any())
            {
                foreach (TreeChain tree in treeList)
                {
                    foreach (var child in tree.GetAllChildren())
                        tree.SetNext(child);

                    var foundObject = tree.Find(value);

                    if (foundObject != null)
                        result.Add(foundObject);
                }

                if (result.Any())
                    return Ok(result.FirstOrDefault());
            }

            return NotFound("No tree was found");
        }

        [HttpPost()]
        public IActionResult Post([FromBody] Tree tree)
        {
            if (tree == null)
                return BadRequest("Invalid tree");

            repository.Add(tree);
            var t = repository.ToList();

            return Ok(new { Value = "Created" });
        }

        [HttpPatch("{treeId}")]
        public IActionResult AddChild(string treeId, [FromBody] Tree child)
        {
            if (child == null)
                return BadRequest("Invalid tree");

            var tree = repository.Find(treeId);

            if (tree == null)
                return NotFound("Tree not found");

            tree.AddChild(child);
            repository.Update(tree);

            return Ok("Tree updated successfully");
        }
    }
}