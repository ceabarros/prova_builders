﻿using Builders.DotNetCore.Domain;
using Builders.DotNetCore.Tests.Integration.Infraestrutura;
using Builders.DotNetCore.Tests.Integration.ResponseData;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using Xunit;

namespace Builders.DotNetCore.Tests.Integration
{
    public class TreeControllerTest : TestBase
    {

        public TreeControllerTest(TestFixture<TestStartup> fixture) : base(fixture)
        { }

        [Fact]
        public async void Get_NoTreeMustReturnNotFound()
        {
            var resultado = await GetStatusCodeGetAsync("Tree");
            Assert.Equal(HttpStatusCode.NotFound, resultado);
        }

        [Fact]
        public async void Get_GivenAtLeastTreeMustReturnCreated()
        {
            var tree = new Tree { Value = 2 };
            var content = await PostAsync<Tree, ResponseContent>("Tree", tree);

            var contentCreated = await GetAsync<IEnumerable<Tree>>("Tree");
            Assert.True(contentCreated.Any());
            Assert.Equal(tree.Value, contentCreated.First().Value);
        }

        [Fact]
        public async void FindWithValue_GivenValueNotRegisteredMustReturnNotFound()
        {
            var value = 0;

            var resultado = await GetStatusCodeGetAsync($"Tree/{value}");
            Assert.Equal(HttpStatusCode.NotFound, resultado);
        }

        [Fact]
        public async void FindWithValue_GivenValueEnteredShouldReturnTree()
        {
            var value = 3;

            var tree = new Tree { Value = value };
            var content = await PostAsync<Tree, ResponseContent>("Tree", tree);

            var resultado = await GetAsync<Tree>($"Tree/{value}");
            Assert.Equal(value, resultado.Value);
        }

        [Fact]
        public async void Post_GivenInvalidTreeMustReturnBadRequest()
        {
            var invalidModel = "{}";

            var model = GetContentData(invalidModel);

            var resultado = (await Client.PostAsync($"/api/Tree", model));

            Assert.Equal(HttpStatusCode.BadRequest, resultado.StatusCode);
        }

        [Fact]
        public async void Post_GivenTreeMustReturnCreated()
        {
            var tree = new Tree { Value = 2 };
            var content = await PostAsync<Tree, ResponseContent>("Tree", tree);
            Assert.Equal("Created", content.Value);
        }

        [Fact]
        public async void AddChild_GivenNotRegisteredTreeMustReturnNotFound()
        {
            var tree = new Tree { Value = 2 };
            var content = await GetStatusCodePutAsync<Tree>("Tree", 1, tree);
            Assert.Equal(HttpStatusCode.NotFound, content);
        }

        [Fact]
        public async void AddChild_GivenValidTreeShouldReturnOK()
        {
            var tree = new Tree { Value = 2 };
            var content = await PostAsync<Tree, ResponseContent>("Tree", tree);

            var contentCreated = await GetAsync<IEnumerable<Tree>>("Tree");

            var id = contentCreated.FirstOrDefault().Id;

            await PatchAsync($"Tree/{id}", new Tree { Value = 5 });
        }


    }
}
