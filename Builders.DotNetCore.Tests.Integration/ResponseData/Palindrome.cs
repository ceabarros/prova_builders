﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Builders.DotNetCore.Tests.Integration.ResponseData
{
    public class PalindromeContent
    {
        public string Word { get; set; }
        public bool Result { get; set; }
    }

    public class ResponseContent
    {
        public string Value { get; set; }
    }
}
