﻿using Builders.DotNetCore.Tests.Integration.ResponseData;
using Builders.DotNetCore.Tests.Integration.Infraestrutura;
using Xunit;
using System.Net;

namespace Builders.DotNetCore.Tests.Integration
{
    public class PalindromeControllerTest : TestBase
    {
        public PalindromeControllerTest(TestFixture<TestStartup> fixture) : base(fixture)
        {
        }

        [Fact]
        public async void GivenPalindromeWordMustBeTrue()
        {
            var word = "Bob";
            var response = await GetAsync<PalindromeContent>($"Palindrome/{word}");
            Assert.True(response.Result);
        }

        [Fact]
        public async void GivenNotPalindromeWordMustBeFalse()
        {
            var word = "Carlos";
            var response = await GetAsync<PalindromeContent>($"Palindrome/{word}");
            Assert.False(response.Result);
        }

        [Fact]
        public async void GvenInvalidWordShouldReturnBadRequest()
        {
            string word = "'";            

            var resultado = await Client.GetAsync($"/api/Palindrome/{word}");
            Assert.Equal(HttpStatusCode.BadRequest, resultado.StatusCode);
        }
    }
}
