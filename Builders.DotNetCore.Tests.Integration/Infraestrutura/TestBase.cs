﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Builders.DotNetCore.Tests.Integration.Infraestrutura
{
    public abstract class TestBase : IClassFixture<TestFixture<TestStartup>>
    {
        public TestBase(TestFixture<TestStartup> fixture)
        {
            Client = fixture.HttpClient;
        }

        protected HttpClient Client { get; private set; }

        public async Task<HttpStatusCode> GetStatusCodeGetAsync(string apiPath)
        {
            var resultado = (await Client.GetAsync($"/api/{apiPath}"));

            return resultado.StatusCode;
        }

        public async Task<TModel> GetAsync<TModel>(string apiPath)
        {
            var resultado = (await Client.GetAsync($"/api/{apiPath}"));

            if (resultado.IsSuccessStatusCode)
                return await ConverterResultadoAsync<TModel>(resultado.Content);

            return default(TModel);
        }

        public async Task<TModel> GetAsync<TModel>(string apiPath, object id)
        {
            var resultado = (await Client.GetAsync($"/api/{apiPath}/{id}"));

            if (resultado.IsSuccessStatusCode)
                return await ConverterResultadoAsync<TModel>(resultado.Content);

            return default(TModel);
        }        

        public async Task<string> GetStringAsync(string apiPath)
        {
            var resultado = (await Client.GetAsync($"/api/{apiPath}"));

            if (resultado.IsSuccessStatusCode)
                return await resultado.Content.ReadAsStringAsync();

            return default(string);
        }

        public async Task<HttpStatusCode> GetStatusCodePostAsync<TModel>(string apiPath, TModel model)
        {
            var contentData = GetContentData(model);

            var resultado = (await Client.PostAsync($"/api/{apiPath}", contentData));

            return resultado.StatusCode;
        }

        public async Task<TModel> PostAsync<TModel>(string apiPath, TModel model)
        {
            var contentData = GetContentData(model);

            var resultado = (await Client.PostAsync($"/api/{apiPath}", contentData));

            if (resultado.IsSuccessStatusCode)
                return await ConverterResultadoAsync<TModel>(resultado.Content);

            return default(TModel);
        }

        public async Task<TReturn> PostAsync<TModel, TReturn>(string apiPath, TModel model)
        {
            var contentData = GetContentData(model);

            var resultado = (await Client.PostAsync($"/api/{apiPath}", contentData));

            if (resultado.IsSuccessStatusCode)
                return await ConverterResultadoAsync<TReturn>(resultado.Content);

            return default(TReturn);
        }

        public async Task<HttpStatusCode> GetStatusCodePutAsync<TModel>(string apiPath, object resourceId, TModel model)
        {
            var contentData = GetContentData(model);

            var resultado = await Client.PutAsync($"/api/{apiPath}/{resourceId}", contentData);

            return resultado.StatusCode;
        }

        public async Task PutAsync<TModel>(string apiPath, object resourceId, TModel model)
        {
            var contentData = GetContentData(model);

            await Client.PutAsync($"/api/{apiPath}/{resourceId}", contentData);
        }

        public async Task PatchAsync<TModel>(string apiPath, TModel model)
        {
            var contentData = GetContentData(model);

            var method = new HttpMethod("PATCH");
            var request = new HttpRequestMessage(method, $"/api/{apiPath}")
            {
                Content = contentData
            };

            await Client.SendAsync(request);
        }

        public async Task PatchFromUri(string path)
        {
            var method = new HttpMethod("PATCH");
            var request = new HttpRequestMessage(method, $"/api/{path}");

            await Client.SendAsync(request);
        }

        public async Task DeleteAsync(string path, object id)
        {
            await Client.DeleteAsync($"/api/{path}/{id}");
        }

        protected StringContent GetContentData<TModel>(TModel model)
        {
            string stringData = JsonConvert.SerializeObject(model);
            return new StringContent(stringData, Encoding.UTF8, "application/json");
        }

        private async Task<T> ConverterResultadoAsync<T>(HttpContent content)
        {
            return await content.ReadAsAsync<T>();
        }
        
    }
}
