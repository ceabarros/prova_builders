﻿using Builders.DotNetCore.Data;
using Builders.DotNetCore.Data.Mongo;
using Builders.DotNetCore.Infrastructure;
using Mongo2Go;
using MongoDB.Driver;
using System;
using System.Threading.Tasks;

namespace Builders.DotNetCore.Tests.Integration.Infraestrutura
{
    public class TestMongoUnitOfWork : UnitOfWork
    {
        private readonly MongoDbRunner _runner;
        private readonly IMongoDatabase _database;
        private readonly MongoClient _client;

        public TestMongoUnitOfWork()
        {
             _runner = MongoDbRunner.Start();
            _client = new MongoClient(_runner.ConnectionString);
            if (_client.IsNotNull())
            {
                _database = _client.GetDatabase("IntegrationTest");
            }
        }

        public Repository<TEntity> GetRepository<TEntity>() where TEntity : class
        {
            return new MongoRepository<TEntity>(_database);
        }

        public void FlushSession()
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            _runner.Dispose();
        }
    }
}
