﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Builders.DotNetCore.Domain
{
    public class Palindrome
    {
        public bool IsPalindrome(string word)
        {
            if (word == null)
                throw new ArgumentException("required value");

            if (word.Length == 1)
                return true;

            var value = word.ToUpper();
            var firstLetter = value.First();
            var lastLetter = value.Last();

            if (firstLetter != lastLetter)
            {
                return false;
            }
            else
            {
                var newWord = word.Substring(1, word.Length - 1);
                newWord = newWord.Substring(0, word.Length - 2);

                return IsPalindrome(newWord);
            }
        }
    }
}
