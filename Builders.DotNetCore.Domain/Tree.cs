﻿using Builders.DotNetCore.Infrastructure;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Builders.DotNetCore.Domain
{
    [DataContract]
    public class Tree : TreeChain
    {
        public Tree()
        {
            Childrens = new List<TreeChain>();
        }        

        public override void AddChild(TreeChain child)
        {
            if (Childrens == null)
                Childrens = new List<TreeChain>();

            if (ICanAdd())
            {
                child.RootValue = Value;
                Childrens.Add(child);
            }
            else
            {
                TreeChain nextChild = null;

                var childrensOrdenados = Childrens.OrderBy(x => x.Value).ToList();

                nextChild = (child.Value < Value) ? childrensOrdenados[0] : childrensOrdenados[1];

                nextChild.AddChild(child);
            }
        }

        private bool ICanAdd() => Childrens.Count < 2;
    }
}
