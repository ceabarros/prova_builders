﻿using Builders.DotNetCore.Infrastructure;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Builders.DotNetCore.Domain
{
    [DataContract(Name = "Tree")]
    public abstract class TreeChain : DomainEntity
    {
        [BsonIgnore]
        public TreeChain Next { get; set; }

        [DataMember(Order = 2)]
        public int Value { get; set; }

        [DataMember(Order = 3)]
        public IList<TreeChain> Childrens { get; set; }

        [DataMember(Order = 1)]
        public override string Id { get => base.Id; set => base.Id = value; }

        public void SetNext(TreeChain tree)
        {
            if (Next == null)
            {
                Next = tree;
            }
            else
            {
                Next.SetNext(tree);
            }
        }

        public TreeChain Find(int value)
        {
            TreeChain tree = null;
            if (Value == value)
            {
                tree = this;
            }
            else
            {
                if (Next != null)
                    tree = Next.Find(value);
            }

            return tree;
        }

        public IList<TreeChain> GetAllChildren()
        {
            var list = new List<TreeChain>();

            foreach (var tree in Childrens)
            {
                var itens = tree.GetAllChildren();
                if (itens.Any())
                    list.AddRange(itens);

                list.Add(tree);
            }

            return list;
        }

        [BsonIgnore]
        public int RootValue { get; set; }

        public abstract void AddChild(TreeChain child);


    }
}
