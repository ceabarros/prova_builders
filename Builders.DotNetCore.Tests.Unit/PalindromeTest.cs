﻿using Builders.DotNetCore.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Builders.DotNetCore.Tests.Unit
{
    public class PalindromeTest
    {
        [Fact]
        public void GivenWordPalindromeShouldReturnTrue()
        {
            var word = "Deleveled";
            var value = new Palindrome().IsPalindrome(word);

            Assert.True(value);
        }

        [Fact]
        public void GivenWordNotPalindromeShouldReturnFalse()
        {
            var word = "test";
            var value = new Palindrome().IsPalindrome(word);

            Assert.False(value);
        }

        [Fact]
        public void GivenWordNullShouldReturnException()
        {
            var palindrom = new Palindrome();

            Assert.Throws<ArgumentException>(() => palindrom.IsPalindrome(null));
        }
    }
}
