﻿using Builders.DotNetCore.Domain;
using Xunit;

namespace Builders.DotNetCore.Tests.Unit
{
    public class TreeChainTest
    {
        [Fact]
        public void GiveTreeINeedSetNext()
        {
            TreeChain tree = new Tree();
            TreeChain treeChild = new Tree();
            tree.SetNext(treeChild);
            Assert.Equal(tree.Next, treeChild);
        }

        [Fact]
        public void GiveValueINeedFindTheTree()
        {
            TreeChain node = new Tree { Value = 5 };
            TreeChain nodeChild1 = new Tree { Value = 6 };
            TreeChain nodeChild2 = new Tree { Value = 7 };
            TreeChain nodeChild3 = new Tree { Value = 8 };
            
            node.SetNext(nodeChild1);
            node.SetNext(nodeChild2);
            node.SetNext(nodeChild3);

            var result = node.Find(7);
            Assert.Equal(nodeChild2, result);
        }

        [Fact]
        public void GivenNonExistentTreeTheResultMustBeNull()
        {
            TreeChain node = new Tree { Value = 5 };
            TreeChain nodeChild1 = new Tree { Value = 6 };
            TreeChain nodeChild2 = new Tree { Value = 7 };
            TreeChain nodeChild3 = new Tree { Value = 8 };

            node.SetNext(nodeChild1);
            node.SetNext(nodeChild2);
            node.SetNext(nodeChild3);

            var result = node.Find(10);
            Assert.Null(result);
        }
    }
}
