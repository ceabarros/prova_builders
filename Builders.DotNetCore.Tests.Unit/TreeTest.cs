﻿using Builders.DotNetCore.Domain;
using Xunit;

namespace Builders.DotNetCore.Tests.Unit
{
    public class TreeTest
    {
        [Fact]
        public void GivenChildINeedAdd()
        {
            var child = new Tree() { Value = 1 };

            var root = new Tree();
            root.AddChild(child);

            Assert.True(root.Childrens.Count == 1);
            Assert.Equal(root.Childrens[0], child);
        }

        [Fact]
        public void GivenTreeShouldOnlyHaveTwoChildren()
        {
            var child1 = new Tree() { Value = 1 };
            var child2 = new Tree() { Value = 2 };
            var child3 = new Tree() { Value = 3 };

            var root = new Tree();
            root.AddChild(child1);
            root.AddChild(child2);
            root.AddChild(child3);

            Assert.True(root.Childrens.Count == 2);
        }

        [Fact]
        public void GivenChildShouldMustAddInMinorParent()
        {
            var child1 = new Tree() { Value = 2 };
            var child2 = new Tree() { Value = 3 };
            var child3 = new Tree() { Value = 3 };
            var child4 = new Tree() { Value = 1 };

            var root = new Tree() { Value = 4 };
            root.AddChild(child1);
            root.AddChild(child2);
            root.AddChild(child3);
            root.AddChild(child4);

            Assert.Equal(child1.Value,child4.RootValue);
        }

        [Fact]
        public void GivenChildShouldAddInTheParentLarger()
        {
            var child1 = new Tree() { Value = 1 };
            var child2 = new Tree() { Value = 3 };
            var child3 = new Tree() { Value = 3 };
            var child4 = new Tree() { Value = 4 };

            var root = new Tree() { Value = 1 };
            root.AddChild(child1);
            root.AddChild(child2);
            root.AddChild(child3);
            root.AddChild(child4);

            Assert.Equal(child3.Value, child4.RootValue);
        }
    }
}
